import React from "react";

import { Col, Row } from "antd";

const DoubleGrid = props => {
    const { container, leftComponent, rightComponent } = props;

    return (
        <Row
            gutter={container.gutter}
            style={container.style}
            className={container.className}
        >
            <Col
                span={
                    leftComponent.span === undefined ? 12 : leftComponent.span
                }
                style={leftComponent.style}
                className={`${
                    leftComponent.justify === undefined
                        ? "left"
                        : leftComponent.justify
                } ${
                    leftComponent.className === undefined
                        ? ""
                        : leftComponent.className
                }`}
            >
                {leftComponent.component}
            </Col>
            <Col
                span={
                    rightComponent.span === undefined ? 12 : rightComponent.span
                }
                style={rightComponent.style}
                className={`${
                    rightComponent.justify === undefined
                        ? "right"
                        : rightComponent.justify
                } ${
                    rightComponent.className === undefined
                        ? ""
                        : rightComponent.className
                }`}
            >
                {rightComponent.component}
            </Col>
        </Row>
    );
};

DoubleGrid.defaultProps = {
    container: {
        gutter: 20
    }
};

export default DoubleGrid;
