import React from "react";

import { Col, Row } from "antd";

const SingleGrid = props => {
    const { container, singleComponent } = props;

    return (
        <Row
            gutter={container.gutter}
            style={container.style}
            className={container.className}
        >
            <Col
                span={24}
                className={`${
                    singleComponent.justify === undefined
                        ? "left"
                        : singleComponent.justify
                } ${
                    singleComponent.className === undefined
                        ? ""
                        : singleComponent.className
                }`}
                style={singleComponent.style}
            >
                {singleComponent.component}
            </Col>
        </Row>
    );
};

SingleGrid.defaultProps = {
    container: {
        gutter: 20
    }
};

export default SingleGrid;
