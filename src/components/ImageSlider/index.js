import React from "react";
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import "./style.css"

const ImageSlider = props => {
    const settings = {
        dots: true,
        autoplay: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        arrows: false
      };

    return (
    <div className="slider-container">
        <div className="container">
            <Slider {...settings}>
            <div><img src={require('./img/ffffff.jpg')} alt="Credit to Joshua Earle on Unsplash"/></div>
            <div><img src={require('./img/ffffff.jpg')} alt="Credit to Alisa Anton on Unsplash"/></div>
            <div><img src={require('./img/ffffff.jpg')} alt="Credit to Igor Ovsyannykov on Unsplash"/></div>
            <div><img src={require('./img/ffffff.jpg')} alt="Credit to Pierre Châtel-Innocenti on Unsplash"/></div>
            <div><img src={require('./img/ffffff.jpg')} alt="Credit to Richard Nolan on Unsplash"/></div>
            <div><img src={require('./img/ffffff.jpg')} alt="Credit to Cristina Gottardi on Unsplash"/></div>
            </Slider>
        </div>
    </div>
    );
};


export default ImageSlider;
