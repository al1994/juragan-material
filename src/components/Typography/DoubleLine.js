import React from "react";
import { Divider } from "antd";
import "./style.css";

const DoubleLineTypography = props => (
    <div
        style={{
            display: "grid"
        }}
        className="double-line-typography"
    >
                <p  style={{
                        marginBottom: -5,
                        color: `${props.title.color ? props.title.color : "black"}`
                    }}
                >
                    {props.title.value}
                </p>

                <h5
                    style={{
                        color: `${props.subtitle.color ? props.subtitle.color : "black"}`
                    }}
                    >
                    {props.subtitle.value}
                </h5>
                <Divider className="divider-content"/>
    </div>
);

export default DoubleLineTypography;
