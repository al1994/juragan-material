import React from "react";
import { FaSearch, FaRocketchat, FaLayerGroup, FaHistory, FaRegUser } from "react-icons/fa";

import "./style.css";

const NavBottom = props => {
    
    const listIcon = [
        {
            icon: <FaSearch size={25}/>,
            title: "SEARCH" 
        },
        {
            icon: <FaRocketchat size={25}/>,
            title: "CHAT" 
        },
        {
            icon: <FaLayerGroup size={25}/>,
            title: "CATEGORY"
        },
        {
            icon: <FaHistory size={25}/>,
            title: "HISTORY"
        },
        {
            icon: <FaRegUser  size={25}/>,
            title: "USER"
        }
         
    ];
    
    return (

        <div className="navbar-bottom">
            {listIcon.map((string) =>
            <>
            <div className="menu-bottom">
                    <div className="icon-menu">{string.icon}</div>
                    <p>{string.title}</p>
            </div>
            </>
            )}
        </div>
    )
}

export default NavBottom;
