import React from "react";
import  { FaMapMarkerAlt, FaHouseDamage, FaCheck } from "react-icons/fa"
import DoubleLineTypography from "../../../components/Typography/DoubleLine";
import DoubleGrid from "../../../components/Grid/DoubleGrid";

import "./style.css";

const Content = props => {

    const listValue = [
        {
            title : "Pick Location",
            subTitle : "Jakarta Selatan",
            icon: <FaMapMarkerAlt size={20}/>
        },
        {
            title : "Type of Property",
            subTitle : "House",
            icon: <FaHouseDamage size={20}/>
        },
        {
            title : "Status",
            subTitle : "Rent",
            icon: <FaCheck size={20}/>
        }
    ]

    return(
        <div className="dashboard-content">
            {listValue.map(function(element){
                return (
                    <DoubleGrid
                        container={{
                            className: "content-container"
                        }}
                        leftComponent={{
                            className: "content-left",
                            span: 4,
                            component: (
                                    element.icon
                            )

                        }}
                        rightComponent={{
                            className: "content-right",
                            span: 20,
                            component: (
                                <DoubleLineTypography
                                title = {{
                                    value: element.title,
                                    size: "tiny"
                                }}
                                subtitle = {{
                                    value: element.subTitle,
                                    size:  "large"
                                }}
                            />
                            )

                        }}
                    />
                )

            })}
        </div>
    )
};

export default Content;
