// import Header from "../../components/Header";
import Content from "./content";
import DoubleGrid from "../../components/Grid/DoubleGrid";
import ImageSlider from "../../components/ImageSlider";
import NavBottom from "../../components/Navigasi/NavBottom";
import  { FaBell, FaHeart } from "react-icons/fa";
import { Button } from "antd";
// import CardMenu from "../../components/Card";
// import { Row} from 'antd';


import "./style.css";


const UI1 = (props) => {

    return (
        <>
            <div className="header">
                <DoubleGrid
                    container={{
                        justify: "left",
                        gutter:0
                    }}
                    leftComponent={{
                        span: 18,
                        component: (
                            <>
                                <p>
                                    REAL ESTATE FINDER    
                                </p>
                            </>

                        )
                    }}
                    rightComponent={{
                        span: 6,
                        justify: "right",
                        component: (
                            <>
                                <DoubleGrid
                                    leftComponent={{
                                        span: 12,
                                        component: (
                                            <FaBell size={20}/>
                                        )
                                    }}
                                    rightComponent={{
                                        span: 12,
                                        component: (
                                            <FaHeart size={20} />
                                        )
                                    }}
                                />
                            </>
                        )
                    }}
                />
            </div>

                <ImageSlider/>

                <Content/>
                
                <div className="button-dashboard">
                    <Button type="primary" block>
                        Find
                    </Button>
                </div>

                <NavBottom
                
                />
        </>
    );
}

export default UI1;